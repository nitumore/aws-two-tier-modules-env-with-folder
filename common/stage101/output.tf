output "vpc_id" {
  value = "${aws_vpc.acme_root_vpc.id}"
}

output "route_table_id" {
  value = "${aws_vpc.acme_root_vpc.main_route_table_id}"
}

output "ig_id" {
  value = "${aws_internet_gateway.acme_root_ig.id}"
}

output "subnet_id" {
  value = "${aws_subnet.acme_web_subnet.id}"
}

output "vpc_security_group_ids" {
  value = "${aws_security_group.acme_web_sg.id}"
}

output "key_id" {
  value = "${aws_key_pair.auth.id}"
}
